/**
 * The class App 
 */
class App {
  // variables
  String name = "", category = "", developer = "";
  int year = 0;

  // Constructor
  App(this.name, this.category, this.developer, this.year);

  String upperCaseName() {
    return name.toUpperCase();
  }

  @override
  String toString() {
    return "The ${this.name} was developed by $developer on ${this.year} and won the $category. ";
  }
}

void main() {
  App app = new App("Fnb Banking App", "Best App of the year",
      'FirstRand Bank Limited', 2012);
  print(app.upperCaseName());
  print(app.toString());
}
