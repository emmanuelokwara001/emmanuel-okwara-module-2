Map<int, List<String>> winners = {
  2012: [
    "fnb banking",
    "discovery healthId",
    "matchy",
    "phrazapp",
    "plascon",
    "rapid targets",
    "transunion dealers guide"
  ],
  2013: [
    "bookly",
    "deloitte digital",
    "dstv",
    "guardian buddy",
    "kids aid",
    "markitshare",
    "nedbank",
    "price check",
    "snapscan"
  ],
  2014: [
    "live inspect",
    "my belonging",
    "rea vaya",
    "supersport",
    "sync mobile",
    "vigo",
    "wildlife",
    "zapper"
  ],
  2015: [
    "cput mobile",
    "dstv now",
    "eskomsepush",
    "m4 jam",
    "vula mobile",
    "wumdrop"
  ],
  2016: [
    "domestly",
    "friendly math monsters for kindergarten",
    "hear za",
    "ikhokha",
    "kaching",
    "mibrand"
  ],
  2017: [
    "OrderIn",
    "intergratme",
    "pick n pay super animals",
    "watif health portal",
    "transunion 1check",
    "hey jude",
    "oru social touch sa",
    "awethu project",
    "zulzi",
    "shyft",
    "eco slips.com"
  ],
  2018: [
    "cowa bunga",
    "acgl",
    "besmarta",
    "xander-english",
    "ctrl",
    "pineapple",
    "bestee",
    "stokfella",
    "african snakebite institute"
  ],
  2019: [
    "digger",
    "si realities",
    "vula mobile",
    "hydra farm",
    "matric live",
    "franc",
    "over",
    "locTransie",
    "naked insurance",
    "my pregnancy journey",
    "loot defence",
    "mowash"
  ],
  2020: [
    "checkers sixty minutes",
    "easy equities",
    "birdpro",
    "technishen",
    "matric live",
    "stokfella",
    "bottles",
    "lexie hearing",
    "league of legends AR",
    "examsta",
    "xitsonga dictionary",
    "green fingers mobile",
    "guardian health",
    "my pregnancy journey"
  ],
  2021: [
    "ambani-afrika",
    "takalot.com",
    "shyft",
    "iiDENTIFii",
    "sisa",
    "guardian health",
    "murimi",
    "uniWise",
    "kazi",
    "rekindle",
    "hellopay",
    "roadsave"
  ],
};

void showdata() {
  winners.forEach((key, value) {
    value.sort();
    print("The winning Apps of $key");
    int count = 1;
    value.forEach((el) => print("${count++} : $el"));
    print("");
  });
}

void main(List<String> args) {
  showdata();
}
